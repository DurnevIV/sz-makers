from django.contrib import admin
from .models import *
from django import forms
from ckeditor_uploader.widgets import CKEditorUploadingWidget

class LetterAdminForm(forms.ModelForm):
    richcontent = forms.CharField(widget=CKEditorUploadingWidget())
    class Meta:
        model = Letter
        fields = "__all__"


class LetterAdmin(admin.ModelAdmin):
    form = LetterAdminForm
    prepopulated_fields = {"slug": ("title",)}


admin.site.register(Letter, LetterAdmin)