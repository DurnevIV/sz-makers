from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^(?P<pk>\d+)/(?P<slug>[-\w]+)/$',
        views.LetterDetailView.as_view(), name='letter-detail'),

]
