from .models import *
from django.shortcuts import render
from django.views.generic.detail import DetailView

# Create your views here.
class LetterDetailView(DetailView):

    model = Letter

    def letter_detail_view_func(request, slug):
        letter = Letter.objects.get(slug=slug)
        context_dict = {
            'letter':letter
        }
        return render(request, 'letters/letter_detail.html', context_dict)
